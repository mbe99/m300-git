[10]: https://gitlab.com
[11]: https://github.com
[20]: https://git-scm.com/
[21]: https://git-scm.com/book/en/v2


# working with Git Commands

Das folgende Dokument ist wie folgt strukturiert:

Im ersten Abschnitt **[Git Repository einrichten](#git-repository-einrichten)** werden die nötigen Schritte erläutert, welche zum Starten mit einem lokalen- und remote Git Repository nötig sind. 

Im zweiten Abschnitt **[Arbeiten mit Git](#arbeiten-mit-git)** geht es darum, die wichtigsten *Handgriffe* im Umgang mit Git zu lernen.

Git ist sehr umfangreich. Für den täglichen Gebrauch reichen aber wenige Kommandos und ein **generelles Verständnis über die Funktionsweise von Git**. Diese sind im **[Pro Git book][21]** (Kapitel 1 und 2) sehr gut beschrieben.  Alle in diesem Dokument verwendeten Kommandos finden sie ebefalls in diesen beiden Kapiteln wieder.

---



## Git Repository einrichten ##

### Git Storage Model

<img style="float: none;width:512px;height:223px;" src="images/Git_StorageDataFlow1.png"> 

* Remote - ist das **entfernte Repository**, oft im Internet, kann aber auch auf dem lokalen Computer sein
* Repository - Repository auf dem **lokalen** Computer
* Index - Staging Area zwischen **Repository** und **Workplace**


### Git Konfigurations Dateien

Git hat folgende Konfigurationsdateien. Jede Ebene überschreibt die obige

* **System** ( --system )
    * `/etc/gitconfig` oder `c:\Program Files\Git\etc`
* **Personal** ( --global )
    * `~/.gitconfig` oder `%HOMEPATH%/.gitconfig`
* **Repository** ( --local )
    * `o	[GIT-REPOSITORY/]/.git/config`


> `git config --list --show-origin` zeigt die aktuelle Konfiguration


Beispiel:

*Username* und *E-Mail* in der **globalen** Konfiguration speichern. Diese werden später bei den Commit Informationen zugefügt. 

```
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```
---
### Starten mit einem neuen Git-Repository

Jetzt geht es darum, wie sie ein *vorhandenes, lokales Verzeichnis* unter **Git Versionskontrolle** bringen und dieses anschliessend in ein *remote Repository* wie [Gitlab][10] oder [Github][11] pushen.

### Remote Git Repository erstellen

Vorbereitend erstellen wir, entweder in [Gitlab][10] oder [Github][11], ein neues, leeres Repository.

> Ein hinterlegter **SSH-PublicKey** des lokalen Benutzers/Rechner ist Voraussetzung

Nachdem wir ein neues Repository in [Gitlab][10] oder [Github][11] erstellt haben, erhalten wir Informationen (passend zum eben erstellten Repository)  über den Import von existierenden lokalen Folder Strukturen in das eben erstellte remote Repsitory. Die Anweisungen sind fast identisch.

#### Github

```
echo "# test" >> README.md
git init
git add README.md
git commit -m "first commit"
git branch -M main
git remote add origin git@github.com:youraccount/test.git
git push -u origin main
``` 

#### Gitlab

```
cd existing_folder
git init
git remote add origin git@gitlab.com:youraccount/test.git
git add .
git commit -m "Initial commit"
git push -u origin master
``` 


Sie können diese Schritte einfach mal analog ihres Repository ausführen und direkt zum Kapitel **Arbeiten mit Git** gehen.

Wenn sie ganz neu mit Git arbeiten und noch wenig Erfahrung haben, können sie nachfolgen einige Erklärungen zu den Schritten erhalten. Die Schritte werden anhand der [Gitlab][10] Anweisungen erläutert.



### lokales Git Repository erstellen

Als Vorbereitung erstellen wir einen neuen *Projekt* Folder, welcher wir danach unter **Git Versionskontrolle** betreiben wollen. Im weiteren Verlauf dieser Anleitung wird dieser Folder `Project_Folder` genannt. Sie können irgendeinen  Namen verwenden. 

Sie können nun im neu erstellten `Project_Folder` Files und Verzeichnisse anlegen, welche später unter Git-Versionskontrolle verwaltet werden sollen. Für diesen Auftrag würde sich beispielsweise folgende Struktur eigenen (welche natürlich jederzeit erweitert und angepasst werden kann).

```

../ProjectFolder$ tree
.
├── git-doc.md
└── images
    └── pic1.jpg
```

Wechseln sie in der *Git Bash* in den eben erstellten Folder.

`$ cd Project_Folder`

Das Repository initialisieren

`Project_Folder> $ git init`

nach dem `git init` Kommando haben wir bereits ein **funktionierendes, lokales Git Repository** erstellt. 

## Repository mit Daten befüllen

### lokales Git Repository mit Remote verbinden

Nun können wir das vorgängig  in [Gitlab][10] oder [Github][11] erstellte **remote Repository** mit dem **lokalen Repository verbinden**, um anschliessend die *lokalen Dateien* in unserem `ProjectFolder` in das *Remote Repository* zu **pushen**. 

Wir verwenden dazu den Befehl `git remote` und erstellen eine Verbindung unter dem Namen `origin`zu unserem remote Repository.

`Project_Folder> $ git remote add origin <ssh-URL>`

`ssh-URL` ersetzen sie durch den **Clone** String von  [Gitlab][10] oder [Github][11] 

<img style="float: none;width:375px;height:200px;" src="images/git-clone.jpg"> 


### Index mit Daten füllen

Im nächsten Schritt fügen wir alle im lokalen Verzeichnis vorhandene Files zum Git Index (Stage) dazu. Der `.`  (Punkt) steht für alles was in diesem Folder und dessen Subfolder sich befindet. Alternativ könnte man auch ein einzelnes File angeben. 

``Project_Folder> $ git add .` 

> mit `git add` werden neue oder geänderte Files dem Index (auch Staging)  zugefügt

### Commit in des lokale Repository

Mit `git add` gelangen die Änderunge erst im Index - sind also bereit (staged) für den Commit aber noch nicht versioniert im lokalen Repository gespeichert. Damit sie tatsächlich in das **lokale Repository** gelangen, braucht es einen **Commit**. Dieser wird mit dem `git commit` Befehl ausgeführt und jeweils einem Kommentar
versehen. Der Kommentar bleibt zu der jeweiligen Änderung erhalten und hilft später die Änderung besser zu verstehen.

`Project_Folder> git commit -m "Initial commit"`


### Push in das remote Repository

Als letzter Schritt führen wir einen **Push** vom lokalen Repository nach **origin** aus. Unter dem Namen origin hatten wir unsere Verbindung zum *remote Repository* definiert. Master steht für den Branche auf dem wir arbeiten. Eine Branche kann als eine Art Release-Version angesehen werden. Solange wir keine weiteren Branches anlegen, arbeiten wir standardmässig immer auf dem **master branche**.

`git push -u origin master`

Nun sehen wir alle lokalen  Files und Verzeichnisse, je nach Remote Repository, in [Gitlab][10] oder [Github][11]

---


## Arbeiten mit Git

### Git Data Model

<img style="float: none;width:500px;height:420px;" src="images/Git_Data_Transport_Commands.jpg"> 


### von der Änderung zum Commit

Der Weg einer Änderung an einem File bis zum Versionierten Commit läuft folgendermassen ab

|command | Workplace | Index (Stage)| local Repository |
|:--:|:--:|:--:|:--:|
|vi,nano,vsc|File Änderung|||
|git add||Änderung staged||
|git commit|||Änderung versioniert|


> mit `git commit -a` kann der Index umgangen werden

---
### Arbeiten im Workplace (lokal)

File oder Verzeichnis unter Git Control löschen

`$ git rm {file} / $ git rm {dir}`

File oder Verzeichnis unter Git Control verschieben

`$ git mv {file} / $ git mv {dir}`

> zum Löschen oder Verschieben immer die **git Kommandos**  `git rm` oder `git mv` verwenden

---
### Arbeiten mit dem Index (stage)

Zeigt den aktuellen File Status

`$ git status`

Neues oder geänderte File(s) der Staging-Area zufügen.

`$ git add {file}`

> `git add .` fügt **alle** neuen oder geänderten Files dem Index zu

Ein versehentlich ge-staged File wieder aus dem Index entfernen

`$ git reset HEAD {file}`

#### Inhalt des Staging ansehen

Zeigt was im Worsplace geändert, aber noch nicht im Index (staged) ist.

`$ git diff`

Zeigt was bereits zum Commit vorgemerkt wurde.

`$ git diff --cached`  (auch -- staged)

---
## Arbeiten mit dem lokalen Repository

Im Index vorgemerkte Änderungen in das lokale Repository committen

`git commit {file}`

Commit Historie ansehen

`$ git log`


Ein im Workplace modifiziertes File wieder auf den Stand des letzter Commit setzen

`$ git checkout -- {file}`

---
## Arbeiten mit dem remote Repository

Remote Repository anzeigen

`$ git remote -v`

Alle Änderungen vom remote Repository (origin) in den Workplace laden

`$ git pull` 

> sind Files lokal und remote geändert, entsteht ein Konflikt

Änderungen vom lokalen Repository nach remote pushen

`git push origin`

> vor einem `push` immer zuerst ein `pull` ausführen

---
## Arbeiten mit Tags

**Tags** sind wie **Snapshots** - sie halten den Zustand zu einem bestimmten Zeitpunkt fest. So können sie jederzeit wieder auf einen Taged Stand ihrer Umgebung zurückgreifen. 

 > Tagged Versionen sind nicht geeignet um an diesen weiter zu arbeiten. Dazu sieht git **Branches** vor, worauf wir hier nicht weiter eingehen werden.

Vorhandene Tags listen

`$ git tag`

Tag erstellen (Annonated)

`$ git tag -a v1.5 -m "Version 1.5"`

> -a erstellt einen Annonated (Kommentierte) Tag

Tag Content ansehen 

`git tag show v1.5`

Einen spezifischen Tag nach origin pushen

`$ git push origin v1.5`

> `git push origin --tags` alle Tags gleichzeitig pushen

Lokale Tags löschen

`$ git tag -d v1.5`

Remote Tags löschen

`$ git push origin --delete v1.5`
